const webpack = require('webpack');

module.exports = {
  chainWebpack: config => {
    config.module
      .rule('svg')
      .use('file-loader')
      .loader('vue-svg-loader')
      .options({
        svgo: {
          plugins: [
            { removeDoctype: true },
            { removeComments: true },
            { cleanupIDs: false },
          ],
        },
      });
  },
  configureWebpack: config => {
    config.optimization = {
      splitChunks: {
        minSize: 10000,
        maxSize: 250000,
      },
    };

    config.plugins.push(
      new webpack.EnvironmentPlugin({
          'CI_COMMIT_REF_NAME': process.env.CI_COMMIT_REF_NAME || '',
      }),
    );

    config.resolve.alias['~'] = '@/components/icons';

    config.module.rules.push({
      test: /\.pug$/,
      oneOf: [
        // this applies to `<template lang="pug">` in Vue components
        {
          resourceQuery: /^\?vue/,
          use: ['pug-plain-loader']
        },
        // this applies to pug imports inside JavaScript
        {
          use: ['raw-loader', 'pug-plain-loader']
        }
      ]
    });
  },
  css: {
    loaderOptions: {
      // pass options to sass-loader
      sass: {
        // @/ is an alias to src/
        // so this assumes you have a file named `src/variables.scss`
        data: `@import "@/variables.scss";`
      },
    },
  },
};
