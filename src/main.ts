import Vue from 'vue';

// @ts-ignore
import VueAnalytics from 'vue-analytics';

// import BootstrapVue from 'bootstrap-vue';
// @ts-ignore
import Alert from 'bootstrap-vue/es/components/alert';
// @ts-ignore
import Badge from 'bootstrap-vue/es/components/badge';
// @ts-ignore
import Button from 'bootstrap-vue/es/components/button';
// @ts-ignore
import ButtonGroup from 'bootstrap-vue/es/components/button-group';
// @ts-ignore
import Form from 'bootstrap-vue/es/components/form';
// @ts-ignore
import FormCheckbox from 'bootstrap-vue/es/components/form-checkbox';
// @ts-ignore
import FormGroup from 'bootstrap-vue/es/components/form-group';
// @ts-ignore
import FormInput from 'bootstrap-vue/es/components/form-input';
// @ts-ignore
import FormTextArea from 'bootstrap-vue/es/components/form-textarea';
// @ts-ignore
import InputGroup from 'bootstrap-vue/es/components/input-group';
// @ts-ignore
import Tooltip from 'bootstrap-vue/es/components/tooltip';
// @ts-ignore
import TooltipDirective from 'bootstrap-vue/es/directives/tooltip';

// @ts-ignore
import VueCookieAcceptDecline from 'vue-cookie-accept-decline';
import 'vue-cookie-accept-decline/dist/vue-cookie-accept-decline.css';

// @ts-ignore
import VueResize from 'vue-resize';

// @ts-ignore
import * as svgicon from 'vue-svgicon';

import ApolloClient from 'apollo-boost';
import VueApollo from 'vue-apollo';

import App from './App.vue';
import Routes from '@/routes';
import { Store } from '@/store';
import config from '@/config';

import 'typeface-lustria';
import 'typeface-poiret-one';
import 'vue-resize/dist/vue-resize.css';
import '@/main.scss';

const is_prod = process.env.NODE_ENV === 'production';
Vue.config.productionTip = false;

const apollo_provider = new VueApollo({
  defaultClient: new ApolloClient({
    uri: `${config.api}/shop`,
  }),
});

const enable_analytics = window.localStorage.getItem('vue-cookie-accept-decline-gdpr') === 'accept';

Vue.use(VueAnalytics, {
  id: 'UA-125465517-1',
  router: Routes,
  disabled: !enable_analytics,
  ecommerce: {
    enabled: true,
    enhanced: true,
  },
  autoTracking: {
    exception: true,
    page: true,
  },
  debug: {
    enabled: false,
    sendHitTask: is_prod,
  },
});

Vue.use(VueApollo);

Vue.use(Alert);
Vue.use(Badge);
Vue.use(Button);
Vue.use(ButtonGroup);
Vue.use(Form);
Vue.use(FormCheckbox);
Vue.use(FormGroup);
Vue.use(FormInput);
Vue.use(FormTextArea);
Vue.use(InputGroup);
Vue.use(Tooltip);
Vue.use(TooltipDirective);

Vue.use(VueResize);

// @ts-ignore
Vue.use(svgicon);

Vue.component('vue-cookie-accept-decline', VueCookieAcceptDecline);

const vm = new Vue({
  router: Routes,
  store: Store,
  apolloProvider: apollo_provider,
  render: (h: any) => h(App),
});

vm.$mount('#app');
