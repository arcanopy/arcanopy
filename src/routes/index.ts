import Vue from 'vue';
import Router from 'vue-router';

import Shop from '@/views/Shop.vue';
import NotFound from '@/views/404.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Shop,
    },
    {
      path: '/privacy-policy',
      component: () => import(/* webpackChunkName: "PrivacyPolicy" */ '@/views/PrivacyPolicy.vue'),
    },
    {
      path: '/about',
      component: () => import(/* webpackChunkName: "About" */ '@/views/About.vue'),
    },
    {
      path: '/shop',
      component: Shop,
    },
    {
      path: '/shop/checkout',
      component: () => import(/* webpackChunkName: "Checkout" */ '@/views/Checkout.vue'),
    },
    {
      path: '/shop/item',
      redirect: '/shop',
    },
    {
      path: '/shop/:collection',
      component: Shop,
      props: route => {
        return {
          collection: route.params.collection,
        };
      },
    },
    {
      path: '/shop/item/:product',
      component: () => import(/* webpackChunkName: "ProductDetails" */ '@/views/ProductDetails.vue'),
      props: route => {
        return {
          handle: route.params.product,
        };
      },
    },
    {
      path: '/newsletter',
      component: () => import(/* webpackChunkName: "NewsletterSubscribe" */ '@/views/NewsletterSubscribe.vue'),
    },
    {
      path: '/404',
      component: NotFound,
    },
    {
      path: '*',
      redirect: '/404',
    },
  ],
});

router.beforeEach((to, from, next) => {
  next();
});

export default router;
