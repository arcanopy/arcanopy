---
title: Philodendron xanadu
subtitle: Care Sheet
---
Unlike most _Philodendrons_, _P. xanadu_ isn't a vine,
instead growing as an upright bush that can take up quite a lot of space.

It has robust, rubbery, glossy-green leaves, which are beautifully serrated at the edges,
and almost seem to attract the light.
It's guaranteed to catch the eye, and needs a spot where it can really spread and grow to its full potential -
as well as a spot where it can get all the attention it deserves!

# Light

Like other _Philodendrons_, it'd enjoy a bright spot where it can get some dappled sunlight,
and be shaded from the harsh direct sun, which can burn the leaves,
but isn't too fussy about light[^1] compared to some plants.

[^1]: This doesn't mean that it can grow in a dark corner that never gets any light!

# Water

The thick leaves mean that _xanadu_ can deal with lower levels of humidity
than most tropical foliage plants, but it loves higher humidity levels -
try giving it a shower occasionally to clean the leaves of dust and to give it a treat!

Be careful of overwatering, and always make sure that the soil is slightly dry before watering again.

# Soil

Likes something with good drainage, but isn't too fussy - any good-quality potting mix, with added perlite, would be ideal.

# Growing Habit

As _Philodendron xanadu_ has a spreading, upright growing habit,
it shouldn't need staking or any support - unlike its vinier cousins,
but would appreciate a big enough space to really spread out.

# Similar To

As a member of the _Philodendron_ family, it requires similar care to most other tropical aroids,
such as _Monstera deliciosa_, _Monstera adansonii_,
the vining _Philodendrons_, and _Pothos_.
