---
title: Anthurium 'Zizou'
subtitle: Care Sheet
---
_Anthurium_ is a collection of aroids.
You probably recognise the varieties that have bright red or pink, glossy, almost plastic-looking inflorescences.
_Anthurium_ 'Zizou' is related to these, probably also a hybrid of _A. andraeanum_, which is where most of the
supermarket and garden centre varieties can trace their wild origins to.

'Zizou' differs from the more common ones in a few aspects -
its leaves are more arrow-shaped than heart-shaped (this might point to some _A. scherzerianum_ influences[^1]),
the flowers - and the whole plant - are a lot smaller, more delicate, less waxy-looking,
with a narrower spathe (the petal-like bit of _Anthurium_ inflorescences, which is actually a modified leaf!),
and a purple spadix (the 'spike' in the middle of the spathe, which is usually yellow in _A. andraeanum_ cultivars).

[^1]: It's almost impossible to trace varities like this back to the original species unfortunately.

# Light

Like most other Aroids, _Anthuriums_ want bright light, but don't need or really enjoy hot direct sunlight.
If it's getting too much light, the leaves will either get pale yellow or show signs of burning,
so just move it away from the light.

# Water

They like to be kept a lot wetter than a lot of other houseplants, but this does **not** mean they want to be kept
in soaking wet soil!
They should be kept in a very loose mix that's really well-draining,
so a regular compost with a high amount of perlite (60-40 ratio) would be ideal.

# Soil

They're not too fussy about fertiliser or anything, but want something with a lot of air pockets!
The airier your soil is, the more you can water without worrying about root rot, and these plants
**love** humidity and water!

# Growing Habit

A lot of _Anthuriums_ are ephiphytes[^2], and have a vining nature, although not as noticeable as
_Monsteras_ or _Philodendrons_. They'll eventually climb out of the pot, and you can choose to either
just repot them deeper, or give them some sort of support, like a moss pole, to climb up.

[^2]: Ephiphytes are plants that grow on other plants for structural support.

# Similar To

Other aroids, such as _Philodendrons_ and _Monsteras_,
although these are usually grown for their foliage instead of their flowers.

A lot of ephiphytic orchids want similar care, and have attractive flowers,
such as _Phalaenopsis_, _Oncidiums_, or _Cattleyas_.
