---
title: Saintpaulia 'Liverpool'
subtitle: Care Sheet
---
_Saintpaulia_, commonly known as African Violet,
is a genus found in cloud forests in tropical eastern Africa. They are one of the only houseplants that
_a)_ flower, and _b)_ can do so all year round.

'Liverpool' inherits the soft, velvety, deep green leaves of _Saintpaulia ionantha_, which most cultivars are bred from,
and has stunning, deep red veins on pale pink undersides.
The pale pink flowers have a slightly ruffled edge, with a ring of deep pink in the centre that highlights the bright yellow
stamens.

# Light

_Saintpaulia_ aren't too fussy about light - they don't want to be in direct sunlight or anywhere too hot,
so in a bright south-facing room (but not at the window!), or the windowsill of a north-facing window would be ideal.

# Water

They like to be kept consistently moist and humid - in the wild they grow in forests that are literally inside clouds,
however, do not not **not** ever water your _Saintpaulia_ from above! Cold water will mark the leaves and flowers,
and in the worst-case, can cause mould and rot!

Instead, water by placing it in a saucer of water and letting it soak up from the bottom.
(I recommend doing this with all houseplants anyway)
Once the soil seems to stop absorbing water - after say, an hour? - pour the excess away.
Don't let it sit in water because the soil will get soggy and cause root rot!

# Soil

Likes something with good drainage, but isn't too fussy. Regular potting soil with a lot of perlite (a 60-40 mix) would be ideal.

# Growing Habit

Small, low-lying plants, they're not gonna grow huge like _Monsteras_, or tall like _Ficus lyrata_.
They can be propagated super easily
through leaf cuttings.[^1]

[^1]: This is exactly what it sounds like - cut a leaf off and stick it in a pot with the stem buried.
In a few months you'll see baby plants poke their way up to the surface!

# Similar To

Other African Violets (there are thousands of varieties!),
care-wise very similar to a lot of orchids, especially ones from similar environments
such as _Draculas_ or _Masdevallias_.

Foliage-wise, some other plants with velvety leaves include
certain varieties of _Calathea_,
some _Philodendron_, like _P. micans_,
the Purple Passion or Velvet Plant, _Gynura aurantiaca_.
