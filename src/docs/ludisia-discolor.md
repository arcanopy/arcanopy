---
title: Ludisia discolor
subtitle: Care Sheet
---
Commonly known as a **jewel orchid**,
_Ludisia discolor_ is usually grown for its
striking foliage, instead of its flowers.

It is a terrestrial orchid,
which means that it grows in the ground,
unlike most orchids - which are epiphytic, meaning they grow on other plants.[^1]

[^1]: This is different to parasitic, as ephiphytes use the plants they grow on for structural support rather than using their host for nutrition.

# Light

Like most orchids, _L. discolor_ is a low-medium light plant.
In their native habitat, they grow on forest floors, so they wouldn't get much light.

# Water

Likes high humidity, doesn't like being allowed to dry completely between waterings,
however this doesn't mean that the soil should be constantly soggy!

# Soil

Likes something with good drainage, but isn't too fussy. Remember - _L. discolor_ is a terrestrial orchid, so doesn't need to be potted in special orchid bark.

# Growing Habit

A creeping growth habit,
will trail over the edge of its pot if not repotted,
and will anchor its horizontal stems where they touch the soil.

# Similar To

_Ludisia_ enjoys growing conditions similar to the other jewel orchids (_Anoectochilus_ and _Macodes_),
which are also grown for their attractive foliage.

It would also grow well with other low-light orchids, such as the slipper orchids, _Paphiopedilum_,
some of which also have beautifully patterned leaves,
or the moth orchids, _Phalaenopsis_,
which are easily available in the UK.
