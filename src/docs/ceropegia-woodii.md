---
title: Ceropegia woodii
subtitle: Care Sheet
---
Commonly known as _String of Hearts_, _C. woodii_ is one of these houseplants that used to be a lot more common,
and is slowly coming back into fashion.

Quick-growing, tolerant of shade, with beautiful heart-shaped leaves, curious, upward-facing flowers, and requiring the absolute minimum of care, it's easy to see why it is popular!

# Light

Although _Ceropegia woodii_ doesn't need the brightest of light to grow,
the more light you can give it, the better-patterened the leaves will be,
and the happier your plant.
Avoid placing in the super-bright midday summer sun - although this shouldn't be much of a problem in the UK!

# Water

The small, succulent leaves, as well as the swollen bead-like growths near the base of the vines are specially adapted to store water.
String of Hearts is successible to root-rot, and likes to be kept on the **dry** side.
You only really need to water when the leaves start to feel a bit soft and look a bit wrinkled, and even then, make sure the soil doesn't stay wet.

# Soil

It isn't fussy about soil, but something that you can successfully keep on the drier side in your growing conditions is ideal.
(Think cactus & succulent mixes, or something with a lot of perlite added for extra drainage)

# Growing Habit

_C. woodii_ is a long, trailing vine, which will occassionally branch out.
If you want a fuller plant, or if yours has gone a bit bald at the top,
just circle the vines back around inside the pot,
so the stems are sitting on the soil,
and they should root in and start growing again.

# Similar To

Other high-light and low-water plants, such as any number of other
cacti and succulents.
For other vining succulents, look out for String of Pearls, or String of Bananas.
