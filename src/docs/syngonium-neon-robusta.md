---
title: Syngonium 'Neon Robusta'
subtitle: Care Sheet
---
_Synngoniums_ are a genus in the aroid family, usually grown for their colourful foliage.
Like a lot of aroids, they have different juvenile and adult leaves, but unlike a lot of aroids,
they're not grown for their adult leaves, instead kept quite small and bushy, and we're encouraged
to pinch out any growth that starts vining
(although if mine vines I'm gonna let it so I can see whether it keeps its colouring when its an adult!)

# Light

They're (like other aroids) bright light plants, but don't want direct sunlight and can handle shade quite well.
As with most variegated plants, especially coloured foliage plants, you want to give them brighter light than,
say a _Monstera_, just to keep the colour vibrant.

# Water

They like high humidity and a reasonable amount of water. I find they're in the family of plants,
like _Calathea_, and _Spathiphyllum_ (peace lilies), that dramatically wilt as soon as they need a drink,
so don't worry too much about not watering them enough, they'll tell you when!

# Soil

Likes something with good drainage, but isn't too fussy. A mix of regular potting soil and perlite,
in roughly a 60/40 mix is a good standard for most plants
(maybe with slow-release fertiliser if you're lazy like me!),
and would be ideal for a _Syngonium_.

# Growing Habit

They seem to stay quite small and bushy (compared to other aroids!),
but I suspect this is because we keep them in smaller pots and don't let them climb.
They spread out into clumps, and seem to be reluctant to start vining,
so are probably closer to the 'self-heading' philodendrons, such as
_P. xanadu_, or 'Congo' varieties.

# Similar To

Other aroids with colourful foliage, like a lot of _Philodendrons_,
also other foliage plants, like _Coleus_, or you could add them to a terrarium setup
with plants such as _Fittonias_.
