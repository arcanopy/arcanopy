---
title: Gynura aurantiaca
subtitle: Care Sheet
---
_Gynura aurantiaca_, is commonly known as the
Velvet plant,
or the Purple Passion plant,
both names that are incredibly well-suited,
as the plant is covered in a layer of velvety-soft purple hair.
Quick-growing, strikingly unusual foliage, easy to propagate,
what's the catch?
Well - the small, and rather pretty, red-to-yellow flowers
unfortunately smell really bad!
But don't let that put you off -
just nip the flower buds out and the problem is solved!

# Light

Give this plant as much light as you can! _G. aurantiaca_ is a relatively high-light plant, and the more light
you can give it, the richer purple the leaves will get.

# Water

This plant is quick to wilt,
which also means that it's quick to tell you when it needs water!
Avoid getting water on the leaves - like other plants with downy leaves (African Violets, for example),
the hairs can trap the water and could lead to disease.

# Soil

_G. aurantiaca_ has no special soil requirements, a high-quality potting compost with added perlite would be ideal.

# Growing Habit

Young plants are bushy, but as the stems get older,
they develop a more trailing habit.
If you want to keep your _G. aurantiaca_ bushy,
pinch the growing tips out after every 3-4 sets of leaves.
It is super easy to take cuttings of, just snip a stem off,
remove the bottom leaves, and put in a glass of water!

# Similar To

It would pair well with other plants that also need
high light and relatively high humidity,
such as African Violets, but it isn't too fussy about its living situation.

The deep purple leaves look striking by themselves,
but would also pair beautifully with the deep green leaves
of other tropical houseplants, such as a _Monstera_ or
_Philodendron_.

Botanically, _Gynura_ is a member of the Daisy family,
however (as far as we know!), it isn't a close relative
to the daisies we know and love.
