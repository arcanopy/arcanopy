---
title: Ficus elastica 'Tineke'
subtitle: Care Sheet
---
_Ficus elastica_, commonly known as the **rubber plant**, because of its rubbery, latex-like white sap,
is one of them plants that always appears on lists such as
_"10 Unkillable Plants That Can Survive Total Darkness"_[^1].
'Tineke' is a variegated form of the common _F. elastica_, having beautiful cream-edged leaves,
with a delicate pink flush when they're new, and requiring almost identical care to the regular form,
being a good all-round plant that can easily grow massive (although it will take a while to do so!).

[^1]: No plants can grow in darkness, and almost all plants would be happier with more light than we usually give them.

# Light

Although _F. elastica_ is tolerant[^2] of lower light levels,
the variegated forms need substantially higher light levels to be happy - as they have a lot less green to photosynthesize with.
'Tineke' is even fussier than the regular variegated form in this aspect, as to get the beautiful pink leaves,
it really needs high light.

[^2]: _F. elastica_ are high-light plants really, they may survive under low-light but they'll sulk.

# Water

_F. elastica_ is quite prone to rotting if it is kept too wet, however does like regular watering.
Always keep it on the drier side (although don't let it dry out!) and make sure it is **never** left sitting in water.

# Soil

Likes something with good drainage, but isn't too fussy. During the growing season, they can be heavy feeders,
so a richer soil (maybe with slow-release fertiliser if you're lazy like me!) would be ideal.

# Growing Habit

_Ficus elastica_ is a large tree in nature, growing 30-40 metres tall!
Because of this, at the sizes that we grow them in our homes,
our plants usually only want to grow a single stem upwards.
Pruning your _Ficus_ can promote branching, and result in a bushier plant,
but when you're cutting the stems, be careful - the sap is incredibly sticky!

# Similar To

Its non-variegated siblings, other large statement plants, such as
_Monstera deliciosa_,
ZZ Plant (_Zamioculcas zamiifolia_),
and snake plants (_Sansevieria_),
although all of these plants have slightly different care!
