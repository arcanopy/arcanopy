---
title: Tillandsia ionantha
subtitle: Care Sheet
---
_Tillandsia_ is a genus of around 650 species, almost all of which are known as **air plants**.
Their name comes from their growing habits - mainly ephiphytic, either not having a root system,
or having roots which are only there to anchor the plant to their growing surface.

_T. ionantha_ is also known as the **blushing bride** plant, turning bright red[^1] when it is getting ready to flower.
The rest of the time, it is an attractive silvery-green colour, going more silver as it dries out.

[^1]: Or other colours in certain cultivars, e.g. _T. ionantha_ 'Druid' blushes yellow.

# Light

All _Tillandsia_ need relatively high levels of light to grow to their full potential,
but avoid hot midday sun as it can burn the leaves.
They will survive under more moderate indoor light levels, but might not be as vibrant.

# Water

As air plants don't have any roots, you can't water them the same you would other plants.
Instead, you can give them a "bath", every week or so, where you leave them in water for a few minutes,
then take out and hang **upside down** to dry, before putting them back in their home.
You can also mist your _T. ionantha_ daily instead of bathing, as it is more tolerant of drier conditions
than other _Tillandsias_.

It's super important to make sure that they dry completely, as any water trapped between the leaves can cause rot.

# Soil

Air plants don't need or want soil.
Some people mount their air plants, either with fishing wire or gluing them to a surface.
If you're going to do this, make sure their base isn't attached to somthing that'll stay constantly wet
(like moss), as this can cause rot.

# Growing Habit

_Tillandsias_ are _Bromeliads_, and like (almost) all _Bromeliads_,
they only flower once in their lives, after which they die
(although this process can take months - and you should care for your plant the same way
you have been doing, even after it's finished flowering!)

They make up for this by the flowers being exceptionally long-lived,
and the original plant will last a long time after flowering,
producing offsets (or "pups") from its base.

# Similar To

Other air plants, _Bromeliads_ and epiphytes (such as most _Orchids_),
although _Tillandsia_ likes higher light levels than a lot of these plants.
They're commonly grown in terrariums with other mounted plants.
