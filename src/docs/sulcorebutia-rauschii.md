---
title: Sulcorebutia rauschii
subtitle: Care Sheet
---
_Sulcorebutia rauscii_ is a species of cactus from South America[^1],
where it enjoys direct sun and hardly any rain.
The species is highly variable, ranging from fresh, crisp green to vivid purple,
the variety we have is bright purple at the top with green undersides to the globes.

Like a lot of plants with this reddish-purple colour, bright light intensifies it.

They need a completely dry, ideally cool (0-10°C) winter dormancy.
Dry to prevent rot, and cool to promote flowering.

[^1]: Fun fact - all cactuses are from America, with the exception of _Rhipsalis baccifera_!
Also - all cactuses are succulents (as a succulent is just a plant with succulent leaves),
but not all succulents are cactuses! (And you can also get succulents from rainforests!)

# Light

Bright bright bright!

The more light you can give it the better!

# Water

Hardly any! None at all during winter, and during the growing season what water it does get
should completely flush the pot out, then drain quickly, letting the soil almost completely dry
out before re-watering.

# Soil

Anything that's got excellent drainage, whether that's a professional cactus mix (with extra perlite/gravel),
a 5-1-1-style or gritty mix[^2], or just regular potting mix with **lots** of added perlite and gravel.

If you're worried about drainage still, then stick to a small pot, and plant in _unglazed_ terracotta,
as it is porous so can help bring extra moisture out of the pot.

[^2]: These mixes come from GardenWeb, now rebranded to Houzz, search for "Al's 5-1-1" or "Al's gritty"
on your favourite search engine, or drop me an email and I can try and find the links for you!

# Growing Habit

_S. rauscii_ is a small, clump-forming cactus, that'll eventually spread out into an impressive
group of individual growths, but will never grow huge or very tall.

# Similar To

Other cactuses and succulents, if you're looking for ones with unique foliage,
maybe try _Lithops_, or _Ledebouria socialis_.
