---
title: Peperomia argyreia
subtitle: Care Sheet
---
Silver bands that look almost like they've been painted on,
along with juicy, deep red petioles[^1] give the **Watermelon Peperomia**
its common name.

Its thick, succulent leaves, as well as its overall tolerance for a wide range of growing conditions, means that this is a great beginner plant.

As an added bonus, like other _Peperomias_, it is super easy to take cuttings from, as it has the ability to grow roots and plantlets from almost every part of the plant!
The easiest way is to take a leaf (with the petiole attached) from the mother plant, let the end dry out for a day or two, and then put it in a little glass of water, and watch and wait!
Keep the water clean and make sure that the leaf isn't allowed to dry out,
and in 1-2 months, you should start to see new growth.

[^1]: This is the name for the stem-like segment that attaches the leaf to the central stem.

# Light

Likes medium light, where it isn't in complete shade all the time, but also isn't exposed to direct sunlight, which can burn the leaves.

# Water

Isn't too fussy about humidity, and although its leaves are succulent-like, it prefers more water than a cactus.
Like a lot of succulent plants, it is prone to root-rot,
so make sure that it is given a chance to dry out between waterings.

# Soil

Likes something well-draining, but isn't particularly fussy. Regular potting soil with added perlite would be ideal.

# Growing Habit

A smallish, compact-growing succulent, _Peperomia argyreia_ is ideal for a windowsill or bookshelf.

# Similar To

The instagram-famous Money Plant (_Pilea peperomioides_),
other _Peperomias_, such as the Raindrop Peperomia.
