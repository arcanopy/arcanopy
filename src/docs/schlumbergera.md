---
title: Schlumbergera
subtitle: Care Sheet
---
Commonly known as the **Christmas**
(or in America, **Thanksgiving**) cactus,
_Schlumbergera_ is a genus of tropical cactuses that grow as ephiphytes or lithophytes.[^1]

[^1]: Ephiphytes are plants that grow on other plants for structural support, and lithophytes are plants that grow on rocks or cliff faces.

# Light

When you hear cactus, you think bright sunlight.
Usually, this'd be ideal, however for _Schlumbergera_ and other tropical cactuses, they actually prefer relatively strong shade, with a minimum of direct light.
(This doesn't mean total darkness - all plants need light to live!)

# Water

Also in direct opposition to almost all other cactuses, _Schlumbergera_ likes high humidity.
The leaves wrinkle when the plant is dehydrated, although care should be taken to make sure the plant doesn't get to that level.
As they are ephiphytic, the roots don't like being kept wet for long periods of time.
I've had good luck with watering mostly by misting the plant until it is dripping wet, letting the water run down the stems into the roots.

# Soil

Likes something with good drainage, but isn't too fussy.
Would probably grow well in orchid bark, as a _kokodema_,
or maybe even mounted on bark.

# Growing Habit

When the plants are younger, they tend to grow strongly upright,
but over time the branches start to gracefully arch.
As the plant gets older, its base will slowly become woody - this is nothing to worry about!

# Similar To

The other tropical cactuses (such as the fishbone cactus and Easter cactus).
It requires the same care as other common ephiphytes, such as _Phalaenopsis_ orchids.
