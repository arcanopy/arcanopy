---
title: Monstera adansonii
subtitle: Care Sheet
---
_Monstera adansonii_ is a tropical vine from South and Central America.
It is commonly grown in the same way _Pothos_ are - as a hanging plant.
Closely related to the (in)famous _Monstera deliciosa_
(also known as the Swiss Cheese Plant), _M. adansonii_ is a much
more manageable size for most homes, as well as being
more unusual!

# Light

Likes bright, indirect light.

# Water

Likes high humidity, but doesn't need a lot of watering.

# Soil

Likes something very well-draining, almost close to an orchid mix.
A mix of mostly bark and perlite would be ideal.

# Growing Habit

Like its bigger cousin, _M. deliciosa_, _M. adansonii_ is a tropical vine.
Like most other tropical vines, if it is allowed to grow up,
instead of trailing down, it will grow bigger leaves,
and it will also have more fenestrations^[This is the fancy science word for the holes in the leaves!]

# Similar To

_M. adansonii_ is an aroid, and requires similar conditions to other aroids,
such as _M. deliciosa_, or _Philodendrons_.
Being a higher-light, higher-humidity plant,
it would also be happy in the same conditions as most warmer-growing orchids, Pothos, or tropical cactuses.

Coming from the _Araceae_ family, it is (probably quite loosely)
related to the Titan Arum,
which has the largest unbranched inflorescence in the world.
(And is mostly famous for smelling of rotting meat),
but luckily, _M. adansonii_ doesn't get anywhere near as big,
or smell of corpses!
