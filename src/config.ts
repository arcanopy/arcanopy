const stage = process.env.CI_COMMIT_REF_NAME || '';

let api = 'http://localhost:3000';

if (stage === 'master') {
  api = 'https://api.arcanopy.com';
}

if (stage === 'dev') {
  api = 'https://api.arcanopy.com/dev';
}

export default {
  api,
};
