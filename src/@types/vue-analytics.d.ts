import _Vue, { PluginFunction } from "vue";
import VueRouter from "vue-router";

declare interface AnalyticsOptions {
  id: string;
  disabled?: boolean;
  router?: VueRouter;
  ecommerce?: {
    enabled: boolean;
    enhanced: boolean;
    options?: any;
  };
  autoTracking?: {
    screenview: boolean;
    exception: boolean;
    page: boolean;
  }
}

declare interface AnalyticsEvent {
  eventCategory: string;
  eventAction: string;
  eventLabel: string;
  eventValue: number;
}

declare interface AnalyticsSocial {
  socialNetwork: string;
  socialAction: string;
  socialTarget: string;
}

declare interface AnalyticsEcommerceBase {
  // The product SKU
  id?: string;
  // The product name
  name?: string;
  brand?: string;
  category?: string;
  variant?: string;
  price?: number;
}

declare interface AnalyticsEcommerceImpression extends AnalyticsEcommerceBase {
  list?: string;
  position?: number;
}

declare interface AnalyticsEcommerceProduct extends AnalyticsEcommerceBase {
  quantity?: number;
  coupon?: string;
  position?: number;
}

declare interface AnalyticsEcommercePromotion {
  id: string;
  name: string;
  creative?: string;
  position?: string;
}

declare interface AnalyticsEcommerceAction {
  id?: string;
  shipping?: number;
  coupon?: string;
  list?: string;
  step?: number;
  option?: string;
}

declare type AnalyticsEcommerceActions =
    'click'
  | 'detail'
  | 'add'
  | 'remove'
  | 'checkout'
  | 'checkout_options'
  | 'purchase'
  | 'refund'
  | 'promo_click'
  ;

declare class VueAnalytics {
  static install(Vue: typeof _Vue, options: AnalyticsOptions): void;

  ecommerce: {
    addProduct(data: AnalyticsEcommerceProduct): void;
    addImpression(data: AnalyticsEcommerceImpression): void;
    setAction(action: AnalyticsEcommerceActions, data?: AnalyticsEcommerceAction): void;
    addPromo(data: AnalyticsEcommercePromotion): void;
  };

  event(data: AnalyticsEvent): void;
  social(data: AnalyticsSocial): void;

  page(path: string): void;

  disable(): void;
  enable(): void;
}
export default VueAnalytics;

declare module "vue-analytics" {

}

declare module "vue/types/options" {
  interface ComponentOptions<V extends _Vue> {
    ga?: VueAnalytics;
  }
}

declare module "vue/types/vue" {
  interface Vue {
    $ga: VueAnalytics;
  }
}
