import Vue from 'vue';
import Vuex from 'vuex';
import { getStoreAccessors } from 'vuex-typescript';
import { VuexPersistence } from 'vuex-persist';
import { Subject } from 'rxjs';
import { AlertOptions } from '@/models/alert';
import { cart } from '@/store/cart';

Vue.use(Vuex);

export interface ActionsContext {
  commit(mutation: string, data?: any): void;
  dispatch(action: string, data?: any): Promise<void>;
}

const _alert = new Subject<AlertOptions>();

const vuex_local = new VuexPersistence({
  storage: window.localStorage,
  modules: ['cart'],
});

export const state = {
  modules: {
    cart,
  },
  getters: {
    alert_observable() {
      return _alert.asObservable();
    },
  },
  mutations: {
    alert(_: any, alert: AlertOptions) {
      _alert.next(alert);
    },
  },
  plugins: [
    vuex_local.plugin,
  ],
};

export const Store = new Vuex.Store<typeof state>(state as any);

const { commit, read, dispatch } = getStoreAccessors<typeof state, typeof state>('');

export const alert_observable = read(state.getters.alert_observable);
export const alert = commit(state.mutations.alert);
