import { state } from '@/store';
import { getStoreAccessors } from 'vuex-typescript';

export const cart = {
  namespaced: true,
  state: {
    checkout_id: null,
  },
  getters: {
    get_checkout_id: (state: any) => state.checkout_id,
  },
  mutations: {
    set_checkout_id(state: any, id: string) {
      state.checkout_id = id;
    },
  },
  actions: {
    set_checkout_id(context: any, id: string) {
      context.commit('set_checkout_id', id);
    },
    async new_checkout_id(context: any, apollo: any): Promise<string> {
      const res = await apollo.mutate({
        mutation: require('@/graphql/new_checkout_id.gql'),
      });
      context.commit('set_checkout_id', res.data.checkoutCreate.checkout.id);
      return res.data.checkoutCreate.checkout.id;
    },
    async get_checkout_id(context: any, apollo: any): Promise<string> {
      if (context.state.checkout_id != null) {
        return context.state.checkout_id;
      }
      return context.dispatch('new_checkout_id', apollo);
    },
  },
};

const {
  commit,
  read,
  dispatch,
} = getStoreAccessors<typeof cart, typeof state>('cart');

export const get_id = dispatch(cart.actions.get_checkout_id);
export const set_id = dispatch(cart.actions.set_checkout_id);
export const new_id = dispatch(cart.actions.new_checkout_id);
