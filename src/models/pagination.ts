export interface Paginated<T> {
  edges: Array<{ node: T, cursor: string }>;
  pageInfo: {
    hasPreviousPage: boolean;
    hasNextPage: boolean;
  };
}

export function defaultPaginated<T>(): Paginated<T> {
  return {
    edges: [],
    pageInfo: {
      hasPreviousPage: false,
      hasNextPage: false,
    },
  };
}

export function arr<T>(arr: Paginated<T> | null): T[] {
  if (arr == null) {
    return [];
  }

  return arr.edges.map(e => e.node);
}

export function has_next(arr: Paginated<any> | null): boolean {
  return arr ? arr.pageInfo.hasNextPage : false;
}

export function cursor(arr: Paginated<any> | null): string | null {
  return arr ? arr.edges[arr.edges.length - 1].cursor : null;
}

export function get<T>(arr: Paginated<T>, i: number): T | undefined {
  const elem = arr.edges[i];
  if (elem == null) {
    return undefined;
  }
  return elem.node;
}
