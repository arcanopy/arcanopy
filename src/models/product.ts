import { Paginated } from '@/models/pagination';
import { CheckoutProduct } from '@/models/checkout';

export interface Currency {
  amount: string;
  currencyCode: string;
}

function is_currency(price: any): price is Currency {
  return price.amount && price.currencyCode;
}

export interface PriceRange {
  minVariantPrice: Currency;
  maxVariantPrice: Currency;
}

function is_price_range(price: any): price is PriceRange {
  return price.minVariantPrice && price.maxVariantPrice;
}

export function format(price: string | Currency | PriceRange): string {
  if (typeof price === 'string') {
    return format({ amount: price, currencyCode: 'GBP' });
  }

  if (is_currency(price)) {
    const currency = price.currencyCode;
    const amount = parseFloat(price.amount);
    return new Intl.NumberFormat('en-GB', { style: 'currency', currency }).format(amount);
  }

  if (price.minVariantPrice.amount === price.maxVariantPrice.amount) {
    return format(price.minVariantPrice);
  }

  return `From ${format(price.minVariantPrice)}`;
}

export interface Image {
  altText?: string;
  original: string;
  small: string;
  medium: string;
}

export interface Product {
  id: string;
  title: string;
  handle: string;
  priceRange: PriceRange;
  images: Paginated<Image>;
  availableForSale: boolean;
}

export interface ProductVariant {
  id: string;
  availableForSale: boolean;
  price: string;
  compareAtPrice: string;
  sku: string;
  weight: number;
  weightUnit: string;
  image?: Image;
  selectedOptions: Array<{
    name: string;
    value: string;
  }>;
  product: {
    handle: string;
  };
}

export interface ProductDetails extends Product {
  descriptionHtml: string;
  variants: Paginated<ProductVariant>;
}

type OptionFn = (v: ProductVariant) => string;

export const colour: OptionFn = v => option(v, 'Colour');
export const pot_size: OptionFn = v => option(v, 'Pot Size', o => `${o}cm`);

export function has_option(v: ProductVariant, name: string): boolean {
  const o = option(v, name, () => 'has');

  return o === 'has';
}

export function option(v: ProductVariant, name: string, cb: (opt: string) => string = o => o): string {
  const opt = v.selectedOptions.find(o => o.name === name);
  if (opt == null) {
    return '';
  }

  return cb(opt.value);
}

export function can_ship(v: ProductVariant | number): boolean {
  const collection_only = 1000;

  if (typeof v === 'number') {
    return v < collection_only;
  }

  return can_ship(v.weight);
}

export function currency(price: string, c: PriceRange | Currency | string): Currency {
  if (is_price_range(c)) {
    return currency(price, c.minVariantPrice);
  }

  if (is_currency(c)) {
    return currency(price, c.currencyCode);
  }

  return {
    amount: price,
    currencyCode: c,
  };
}

export function handle(p: Product | ProductVariant | CheckoutProduct): string {
  function is_product(p: any): p is Product {
    return p.title && p.handle;
  }

  function is_variant(p: any): p is ProductVariant {
    return p.product && p.product.handle;
  }

  function is_checkout(p: any): p is CheckoutProduct {
    return p.quantity && p.variant;
  }

  if (is_product(p)) {
    return p.handle;
  }

  if (is_variant(p)) {
    return p.product.handle;
  }

  if (is_checkout(p)) {
    return p.variant.product.handle;
  }

  const _: never = p;
  return '';
}

export function get_image_url(image: Image | undefined, width?: 'small' | 'medium' | 'original'): string {
  const xs = 576;

  if (image == null) {
    return '';
  }

  if (width != null) {
    return image[width];
  }

  return window.screen.width > xs ? image.small : image.medium;
}
