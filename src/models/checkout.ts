import { Paginated } from '@/models/pagination';
import { ProductVariant, Currency } from '@/models/product';

export interface CheckoutProduct {
  id: string;
  title: string;
  quantity: number;
  variant: ProductVariant;
}

export interface Checkout {
  id: string;
  webUrl: string;
  completedAt?: string;
  lineItems: Paginated<CheckoutProduct>;
  subtotalPrice: string;
  totalPrice: string;
}

export function price(product: CheckoutProduct): Currency {
  const num = parseFloat(product.variant.price);

  return {
    amount: `${num * product.quantity}`,
    currencyCode: 'GBP',
  };
}
