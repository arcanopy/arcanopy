export enum AlertStatus {
  Primary,
  Secondary,
  Success,
  Danger,
  Warning,
  Info,
  Light,
  Dark,
}

export namespace AlertStatus {
  export function toClass(status?: AlertStatus): string {
    if (status == null) {
      status = AlertStatus.Primary;
    }

    const status_name = AlertStatus[status].toLowerCase();
    return `alert alert-${status_name} alert-dismissible fade show`;
  }
}

export interface AlertOptions {
  permanent?: boolean;
  status?: AlertStatus;
  timeout?: number;
  title?: string;
  text: string;
}
