export interface CollectionName {
  id: string;
  title: string;
  handle: string;
}
