src_dir=/tmp/google-fonts
dst_dir=/usr/share/fonts/truetype/google-fonts
curr=$PWD
mkdir -p $src_dir
mkdir -p $dst_dir
cd $src_dir
wget 'https://github.com/google/fonts/raw/master/ofl/poiretone/PoiretOne-Regular.ttf'
wget 'https://github.com/google/fonts/raw/master/ofl/lustria/Lustria-Regular.ttf'
find $src_dir -type f -name "*.ttf" -exec install -Dm644 {} $dst_dir \;
fc-cache -f > /dev/null

cd $curr

markdown=()
while IFS=  read -r -d $'\0'; do
  markdown+=("$REPLY")
done < <(find ./src/docs -type f -name '*.md' -printf '%p\n%f\0')

pandoc -v

for i in "${markdown[@]}"; do
  mapfile -t md <<< "$i"
  dst="${md[0]#"./src/docs"}"
  dst="$(dirname "$dst")"
  dst="${dst%"/"}"

  src="${md[0]}"
  name="$(basename "${md[1]}" .md)"

  dst="./public/docs$dst"
  mkdir -p "$dst"
  echo "$src"
  pandoc -s "$src" -o "$dst/$name.pdf" --pdf-engine=lualatex --template "./src/docs/template.tex"
done
